
#ifndef __GRID_GA__
#define __GRID_GA__

#include "ga.h"

namespace GA {
namespace GRID {

typedef enum _GRID_STATUS {
    NotSubmitted,
    Queued,
    Running,
    Failed,
    Finished
} GridStatus;

class GridJobChromosome : public Chromosome {
protected:
    int grid_id;

    virtual double Evaluate();

    virtual void CloneBase(Chromosome * cloneTo) {
        Chromosome::CloneBase(cloneTo);
    }

public:

    GridJobChromosome() : Chromosome(), grid_id(-1) {}

    /// Required from the Chromosome interface
    virtual Chromosome * Clone() = 0;
    virtual void Generate() = 0;
    virtual void Mutate() = 0;

    virtual void SetupJob() = 0;
    virtual GridStatus SubmitJob() = 0;
    virtual GridStatus GetStatus() = 0;
    virtual bool CheckOutput() = 0;
    virtual double ReadResult() = 0;
};

typedef void(*SetupBatchMethod)(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);
typedef int(*SubmitBatchMethod)(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);
typedef GridStatus(*CheckBatchStatusMethod)(int grid_id);
typedef bool(*BatchCompleteMethod)(int grid_id, const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);

void GridBatchEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);

}
}

#endif
