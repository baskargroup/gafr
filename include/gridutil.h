#ifndef __GRID_UTIL__
#define __GRID_UTIL__

#include "gridga.h"

namespace GRIDUTIL {

/// General function to call the system's
/// shell and retrieve the output.
std::string sh(const char * cmd);

/// PBS Style helper functions
int PbsSubmitJob(std::string script);
GA::GRID::GridStatus PbsJobState(int jobid);
void PbsKillJob(int jobid);

/// SLURM Style helper functions
int SlurmSubmitJob(std::string script, std::string dir);
GA::GRID::GridStatus SlurmJobState(int jobid);
void SlurmKillJob(int jobid);

}

#endif