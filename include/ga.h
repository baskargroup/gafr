#ifndef __GA__
#define __GA__

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <exception>
#include <mutex>
#include <functional>

namespace GA {

/**
 * Singleton class that wraps up the dSFMT into a object that can be called
 * by client code
 */
class Random
{
    public:
        static void Reset(unsigned int _seed) { Instance().ResetInternal(_seed); }
        static double GetRandom() { return Instance().GetRandomInternal(); }
        static double GetRandomGaussian(double mu, double sigma) { return Instance().GetRandomGaussianInternal(mu, sigma); }
        static unsigned int GetUsedSeed() { return Instance().seed; }

    private:
        unsigned int seed;

        static Random& Instance()
        {
            static Random instance;
            return instance;
        }

        Random();

        void ResetInternal(unsigned int _seed);
        double GetRandomInternal();
        double GetRandomGaussianInternal(double mu, double sigma);

        Random(Random const&) = delete;
        void operator=(Random const&) = delete;

};

/**
 * A setting for the GA. More types may be added later.
 */
typedef enum {
    TYPE_BOOL,
    TYPE_INT,
    TYPE_DOUBLE,
    TYPE_STRING,
    TYPE_POINTER
} SettingType;

typedef struct {
    SettingType type;
    union {
        bool AsBool;
        int AsInt;
        double AsDouble;
        void * AsPointer;
    };
} Setting;

/**
 * The configuration for a solver. A solver contains a configuration.
 */
class Configuration {
private:
    /// Internal container for setting values.
    std::map<std::string, Setting> Settings;

    /// mutex lock to provide thread safety.
    mutable std::mutex lock;

public:
    Configuration() : Settings(), lock() { }
    Configuration(const Configuration& other) : Settings(other.Settings), lock() { }

    void operator=(const Configuration& other) {
        lock.lock();
        this->Settings = other.Settings;
        lock.unlock();
    }

    /// Used to retrieve a setting from the configuration.
    const Setting GetSetting(std::string key, bool doThrow = true) const;

    /// Used to check at run-time if the configuration has a setting.
    bool HasSetting(std::string key) const;

    /// Prints the entire configuration to stdout or stderr
    void PrintConfiguration(bool use_cerr = true) const;

    /// Specialized methods to handle different setting types.
    void SetSetting(std::string key, Setting& val, bool replace = true);
    void SetSetting(std::string key, bool val, bool replace = true);
    void SetSetting(std::string key, int val, bool replace = true);
    void SetSetting(std::string key, double val, bool replace = true);
    void SetSetting(std::string key, void * val, bool replace = true);
};

/**
 * Base class interface for a chromosome. This must be implemented and
 * used as a type argument to Solver for things to work properly.
 */
class Chromosome {
private:
    /// Used to determine if this chromosome
    /// has already been evaluated so that repeat
    /// calls to Evaluate are avoided.
    bool scored;

    /// The cached score.
    double score;

protected:
    /// This chromosomes ID given by the solver.
    int id;

    /// Evaluation method to be supplied by client code.
    virtual double Evaluate() = 0;

    /// Set to point to the solver's configuration so client
    /// code has access to configuration at run-time.
    Configuration * config;
    
    /// When running threaded evaluation, the thread setup
    /// method is called to produce a thread-local struct
    /// for use in that thread. It is bound to this object
    /// before Evaluate() is called.
    void * thread_data;

    /// Used to clone the base of a chromosome. A call to
    /// CloneBase() is REQUIRED by client code in order for
    /// the Solver to operator properly.
    virtual void CloneBase(Chromosome * cloneTo) {
        cloneTo->SetConfiguration(config);
        cloneTo->SetScore(score);
    }

public:
    Chromosome() : scored(false), score(0.0), config(0) {}

    /// Used internally by Solver to point the chromosomes to
    /// configuration of the solver they are contained by.
    void SetConfiguration(Configuration * _config) { config = _config; }
    
    /// Used internally by colver to point the chromosomes to
    /// thread-local data of the thread they are run on.
    void SetThreadData(void * _thread_data) { thread_data = _thread_data; }

    /// Used to set the score for use when cloning.
    void SetScore(double _score) { score = _score; scored = true; }

    /// Used to clear the score for use when mutating.
    void ClearScore() { scored = false; }

    /// Clone is REQUIRED to be implemented by client code.
    virtual Chromosome * Clone() = 0;

    /// Generate is REQUIRED to be implemented by client code.
    /// It must by some means initialize the chromosome representation
    /// with random data. This is used to form the initial population.
    virtual void Generate() = 0;

    /// A score caching mechinisim so that Evaluate() is only called once
    /// by the system. This is to ensure that for whatever selection /
    /// crossover methods are used, repeat evaluations are not performed.
    double GetScore() {
        if(scored)
            return score;

        score = Evaluate();

        scored = true;
        return score;
    }

    /// This determines if the current chromosome has been evaluated.
    bool Scored() { return scored; }

    /// This is used to set the chromosome ID for use in the Solver
    /// system. Client code MUST NOT modify the ID given by the Solver.
    void SetId(int _id) { id = _id; }

    /// This returns the ID set by the solver in some cases where client
    /// code should need to identify the chromosomes during the Solving
    /// process.
    int GetId() { return id; }

    /// Mutation method is REQUIRED by client code.
    virtual void Mutate() = 0;

    /// Client code should make sure to clean up after itself.
    virtual ~Chromosome() { }
};

/// Template for allocator function REQUIRED by client code.
typedef Chromosome*(*ChromosomeAllocator)(void);

/// typedef for a generation (Makes code easier to read)
typedef std::vector<int> Generation;

/**
 * Collection of predefined methods of evaluating the population
 */
namespace Evaluation {
/// Template of any evaluation method. Client code may optionally
/// use built-in evaluation methods, or implement their own.
typedef void(*EvaluationMethod)(const Configuration&, std::vector<Chromosome *>&, Generation&);

/// Template of thread_data constuction method
typedef std::function<void*(int)> CreateNewThreadData;

/// This simply loops through the chromosomes in a system and calls GetScore() on them
/// one at a time (I.e. no parallelism)
void SerialEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);

/// This will perform the GetScore() calls in parallel. The integer setting "numThreads"
/// must be supplied by client code for this to work properly. numThreads tells the Solver
/// how many threads to create to do parallel operations.
void ParallelEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);
}

/**
 * For the sake of having logic to my scoping, I am placing
 * the typedef for a crossover method in a name namespace
 */
namespace Crossover {
/// Template of a crossover method. Client code is REQUIRED to supply
/// a specialized crossover methods for its chromosome representation.
typedef void(*CrossoverMethod)(const Configuration&, Chromosome *, const Chromosome *, const Chromosome *);
}

/**
 * Collection of predefined selection methods for use by client code
 */
namespace Selection {
/// Template of a selection methods. Similar to evaluation methods, client code
/// may choose to make use of built-in selection methods or choose their own.
typedef Chromosome*(*SelectionMethod)(const Configuration&, const std::vector<Chromosome *>&, Generation&);

/// This will perform a tournament selection with k being supplied in the Solver's config
/// as "TournamentSize". This is the default selection method.
Chromosome* TournamentSelect(const Configuration& config, const std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);

/// This will perform either a score weighted, or rank weighted roulette wheel selection.
/// The configuration variable "RouletteWheelRanked" can be set to true or false to achieve this.
Chromosome* RouletteWheelSelect(const Configuration& config, const std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration);
}

typedef void(*PostStepCallback)(Configuration& config);

/**
 * Represents a defined GA that can be run. The solver is responsible
 * for maintaining a valid configuration and running the GA. A Solver
 * will NOT verify that any custom client settings (that make use of
 * the configuration) are coherent.
 */
class Solver {
private:
    /// Container of chromosomes. Pointers are used since
    /// a chromosome is an abstract interface and templates
    /// can't easily be packed into a library.
    std::vector<Chromosome *> Chromosomes;

    /// A generation is a list of integer indices to the global
    /// chromosome container. This allows two generations to share
    /// the same chromosome without the need for clones to be made
    /// (In the case of elites).
    std::vector<Generation> Generations;


    /// The global GA configuration. This is a run-time configuration
    /// managed by the solver. The user can modify default GA settings
    /// and optionally provide their own global settings that can be
    /// used with their chromosome implementation.
    Configuration config;

    /// An internal methods to fill the required default settings of the GA.
    void FillDefaultSettings();

    /// [deprecated] For internal use of getting a setting value
    Setting& GetSetting(std::string key, bool doThrow = true);

public:
    Solver();
    Solver(const Configuration& templateConfig);
    Solver(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator);
    Solver(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator, const Configuration& templateConfig);

    /// This allows public access to the Solver's configuration so
    /// client code can modify it.
    Configuration& Config() { return config; }

    /// This performs one single step of the GA
    void GAStep(bool printTop);

    /// This runs the Solver until either the
    /// stall limit is reached, or max generation
    /// limit is reached.
    void Run(bool printTop);

    /// Used to provide access to the top scoring chromosome
    Chromosome * GetTop() { return Chromosomes[Generations[config.GetSetting("GenerationNum").AsInt - 1].back()]; }

    /// Used to remove the top scoring chromosome from the
    /// container so that it won't be destroyed when the
    /// solver is.
    Chromosome * PopTop() {
        Chromosome * Top = GetTop();
        Chromosomes[Generations[config.GetSetting("GenerationNum").AsInt - 1].back()] = 0;
        return Top;
    }

    /// Used to provide public access to the
    /// current generation.
    int GetCurGen() { return config.GetSetting("GenerationNum").AsInt; }

    ~Solver() {
        for(Chromosome * ch : Chromosomes) {
            if(ch)
                delete ch;
        }
    }
};

}

#endif
