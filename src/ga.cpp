#include "ga.h"

#include <algorithm>

namespace GA {
    
struct ChromosomeComparer {
    std::vector<Chromosome *> * Chromosomes;
    bool operator()(int a, int b) { return ((*Chromosomes)[a]->GetScore() < (*Chromosomes)[b]->GetScore()); }	
    
    ChromosomeComparer(std::vector<Chromosome *> * _Chromosomes) : Chromosomes(_Chromosomes) { }
};
    


Solver::Solver() : Chromosomes(), Generations(), config() {
    FillDefaultSettings();
}

Solver::Solver(const Configuration& templateConfig) : Chromosomes(), Generations(), config(templateConfig) {
    FillDefaultSettings();	
}

Solver::Solver(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator) : Chromosomes(), Generations(), config() {
    FillDefaultSettings();
    config.SetSetting("CrossoverMethod", (void *) crossover, true);
    config.SetSetting("ChromosomeAllocator", (void *) allocator, true);
    config.SetSetting("PopulationSize", populationSize, true);		
}

Solver::Solver(int populationSize, Crossover::CrossoverMethod crossover, ChromosomeAllocator allocator, const Configuration& templateConfig) : Chromosomes(), Generations(), config(templateConfig) {
    FillDefaultSettings();
    config.SetSetting("CrossoverMethod", (void *) crossover, true);
    config.SetSetting("ChromosomeAllocator", (void *) allocator, true);
    config.SetSetting("PopulationSize", populationSize, true);		
}

void Solver::FillDefaultSettings() {
    config.SetSetting("PopulationSize", 100, false);
    config.SetSetting("MaxGenerations", 200, false);
    config.SetSetting("OutputUnchangedLimit", 50, false);
    config.SetSetting("MutationRate", 0.05, false);
    config.SetSetting("SelectionMethod", (void *) Selection::TournamentSelect, false);
    config.SetSetting("TournamentSize", 4, false);
    config.SetSetting("EvaluationMethod", (void *) Evaluation::SerialEvaluate, false);
    config.SetSetting("ElitePart", 0.05, false);
    config.SetSetting("GenerationNum", 0, false);
}

void Solver::GAStep(bool printTop = false) {
    // Read needed items from the configuration
    int PopSize = config.GetSetting("PopulationSize").AsInt;
    int GenerationNum = config.GetSetting("GenerationNum").AsInt;
    Selection::SelectionMethod select = (Selection::SelectionMethod) config.GetSetting("SelectionMethod").AsPointer;
    Crossover::CrossoverMethod crossover = (Crossover::CrossoverMethod) config.GetSetting("CrossoverMethod").AsPointer;
    Evaluation::EvaluationMethod evaluate = (Evaluation::EvaluationMethod) config.GetSetting("EvaluationMethod").AsPointer;
    ChromosomeAllocator allocator = (ChromosomeAllocator) config.GetSetting("ChromosomeAllocator").AsPointer;
    PostStepCallback step_callback = nullptr;
    if (config.HasSetting("StepCallback")) {
        step_callback = (PostStepCallback) config.GetSetting("StepCallback").AsPointer;
    }
    double ElitePart = config.GetSetting("ElitePart").AsDouble;
    double MutationRate = config.GetSetting("MutationRate").AsDouble;
    
    int NumElites = PopSize * ElitePart;

    if(GenerationNum == 0) {
        // Create initial population
        Generations.push_back(Generation());
        
        for(int i = 0; i < PopSize; i++) {
            Chromosome * ch = allocator();
            ch->SetConfiguration(&config);
            ch->Generate();
            Chromosomes.push_back(ch);
            Generations[0].push_back(Chromosomes.size() - 1);
            ch->SetId(Chromosomes.size() - 1);
        }
    }
    
    
    Generations.push_back(Generation());
    Generation& CurrentGeneration = Generations[Generations.size() - 2];
    Generation& NextGeneration = Generations[Generations.size() - 1];
    
    // Evaluate
    evaluate(config, Chromosomes, CurrentGeneration);

    // Sort previous population so we can retrieve top results
    ChromosomeComparer comparer = ChromosomeComparer(&Chromosomes);
    std::sort(CurrentGeneration.begin(), CurrentGeneration.end(), comparer);

    if (printTop)
        std::cerr << "Top Score for generation " << GenerationNum << " : " << Chromosomes[CurrentGeneration.back()]->GetScore() << std::endl;
    
    // Selection, Crossover, and Mutation
    int NumMutation = (PopSize - NumElites) * MutationRate;
    int NumCrossover = PopSize - NumMutation - NumElites;
    
    // Mutation Children
    for(int i = 0; i < NumMutation; i++) {
        Chromosome * ch = select(config, Chromosomes, CurrentGeneration)->Clone();
        ch->ClearScore();
        ch->Mutate();
        Chromosomes.push_back(ch);
        NextGeneration.push_back(Chromosomes.size() - 1);
        ch->SetId(Chromosomes.size() - 1);
    }
    
    // Crossover Children
    for(int i = 0; i < NumCrossover; i++) {
        Chromosome * ch = allocator();
        ch->SetConfiguration(&config);
        crossover(config, ch, select(config, Chromosomes, CurrentGeneration), select(config, Chromosomes, CurrentGeneration));
        Chromosomes.push_back(ch);
        NextGeneration.push_back(Chromosomes.size() - 1);
        ch->SetId(Chromosomes.size() - 1);
    }
    
    // Elite Children
    for(int i = PopSize - NumElites; i < PopSize; i++) {
        NextGeneration.push_back(CurrentGeneration[i]);
    }

    config.SetSetting("GenerationNum", GenerationNum + 1);

    if (step_callback) step_callback(config);
}


void Solver::Run(bool printTop = false) {
    int MaxGenerations = config.GetSetting("MaxGenerations").AsInt;
    int OutputUnchangedLimit = config.GetSetting("OutputUnchangedLimit").AsInt;
    int GenerationNum = config.GetSetting("GenerationNum").AsInt;
    int OutputUnchanged = 0;
    double TopScore = -1.0;
    while(GenerationNum < MaxGenerations) {
        GAStep(printTop);
        
        if(TopScore == GetTop()->GetScore()) {
            OutputUnchanged++;
        }
        else {
            OutputUnchanged = 0;
            TopScore = GetTop()->GetScore();
        }
        
        if(OutputUnchanged > OutputUnchangedLimit) {
            if (printTop)
                std::cerr << "Warning: Output unchanged for " << OutputUnchangedLimit << " generations. Exiting." << std::endl;
            break;
        }

        GenerationNum = config.GetSetting("GenerationNum").AsInt;
    }	
}

}
