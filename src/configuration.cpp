#include "ga.h"

namespace GA {
void Configuration::SetSetting(std::string key, bool val, bool replace) {
    Setting s;
    s.type = TYPE_BOOL;
    s.AsBool = val;
    SetSetting(key, s, replace);
}

void Configuration::SetSetting(std::string key, int val, bool replace) {
    Setting s;
    s.type = TYPE_INT;
    s.AsInt = val;
    SetSetting(key, s, replace);
}

void Configuration::SetSetting(std::string key, double val, bool replace) {
    Setting s;
    s.type = TYPE_DOUBLE;
    s.AsDouble = val;
    SetSetting(key, s, replace);
}

void Configuration::SetSetting(std::string key, void * val, bool replace) {
    Setting s;
    s.type = TYPE_POINTER;
    s.AsPointer = val;
    SetSetting(key, s, replace);
}

// TODO:
// void Configuration::SetSetting(std::string key, std::string val, bool replace) {
//     Setting s;
//     s.AsString = TYPE_STRING;
//     SetSetting(key, s, replace);
// }

void Configuration::SetSetting(std::string key, Setting& val, bool replace) {
    lock.lock();

    if ((Settings.count(key) <= 0) || replace) {
        Settings[key] = val;
    }

    lock.unlock();	
}


bool Configuration::HasSetting(std::string key) const {
    lock.lock();
    bool hasKey = Settings.count(key) > 0;
    lock.unlock();
    
    return hasKey;
}

void Configuration::PrintConfiguration(bool use_cerr) const {
    auto& outs = use_cerr?std::cerr:std::cout;
    for (auto& kv : Settings) {
        outs << kv.first;
        outs << " = ";

        switch (kv.second.type) {
        case TYPE_BOOL:
            if (kv.second.AsBool)
                outs << "true";
            else
                outs << "false";

            outs << " (Bool Value)";
            break;

        case TYPE_INT:
            outs << std::dec << kv.second.AsInt << " (Int Value)";
            break;

        case TYPE_DOUBLE:
            outs << std::dec << kv.second.AsDouble << " (Double Value)";
            break;

        // case TYPE_STRING:
        //     outs << kv.second.AsString << " (String Value)";
        //     break;

        case TYPE_POINTER:
            outs << std::hex << kv.second.AsPointer << " (Pointer Value)";
            break;

        default:
            outs << std::hex << kv.second.AsPointer << " (Unknown Value)";
            break;
        }

        outs << std::endl;
    }
}

const Setting Configuration::GetSetting(std::string key, bool doThrow) const {
    static Setting nullSetting;
    
    if (!HasSetting(key)) {
        std::cerr << "Warning: Failed to find value for setting \"" << key << "\"" << std::endl;

        if(doThrow)
            throw std::runtime_error(std::string("No value for setting \"") + key + std::string("\""));
        else
            return nullSetting;
    }
    
    lock.lock();
    Setting s = Settings.at(key);
    lock.unlock();
    
    return s;	
}
    
}