#include "ga.h"

namespace GA {
namespace Evaluation {

void SerialEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration) {
    for(int idx : CurrentGeneration) {
        Chromosomes[idx]->GetScore();
    }
}



}
}
