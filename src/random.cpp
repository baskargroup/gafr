#include <iostream>
#include <time.h>
#include <cstdlib>
#include <cmath>
#include <limits>

#include "dSFMT.h"
#include "ga.h"

namespace GA {

static dsfmt_t dsfmt;

Random::Random() : seed(time(NULL)) {
    dsfmt_init_gen_rand(&dsfmt, seed);
}

void Random::ResetInternal(unsigned int _seed) {
    seed = _seed;
    dsfmt_init_gen_rand(&dsfmt, _seed);
}

double Random::GetRandomInternal() {
    double r = dsfmt_genrand_close_open(&dsfmt);
    return r;
}

double Random::GetRandomGaussianInternal(double mu, double sigma) {
    const double epsilon = std::numeric_limits<double>::min();
    const double two_pi = 2.0*3.14159265358979323846;

    static double z0, z1;
    static bool generate = false;
    generate = !generate;

    if (!generate)
        return z1 * sigma + mu;

    double u1, u2;
    do
    {
        u1 = GetRandomInternal();
        u2 = GetRandomInternal();
    } while (u1 <= epsilon);

    z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
    z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
    return z0 * sigma + mu;
}

}