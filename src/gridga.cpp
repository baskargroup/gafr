#include "gridga.h"

#include <chrono>
#include <exception>
#include <thread>
#include <stdlib.h>
#include <unistd.h>

namespace GA {
namespace GRID {

double GridJobChromosome::Evaluate() {
    int retry_count = this->config->GetSetting("grid_retry_count").AsInt;

    GridStatus Status;

    this->SetupJob();

    while (retry_count > 0) {
        
        Status = this->SubmitJob();


        while (Status == Queued || Status == Running) {
            sleep(1);
            Status = this->GetStatus();
        }

        if (this->CheckOutput()) {
            break;
        }
     
        std::cerr << "Chromosome " << this->id << " failed grid execution." << std::endl;
        std::cerr << "Failing chromosome " << this->id << " retrying..." << std::endl;   

    }

    if (retry_count == 0) {
        std::cerr << "Chromosome " << this->id << " failed grid execution." << std::endl;
        std::cerr << "Failing chromosome " << this->id << " (retry limit exceeded)." << std::endl;

        return -1.0;
    }

    return this->ReadResult();
}

void GridBatchEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration) {
    int retry_count = config.GetSetting("grid_retry_count").AsInt;
    int grid_id;
    GridStatus Status;

    SetupBatchMethod SetupBatch = (SetupBatchMethod) config.GetSetting("SetupBatchMethod").AsPointer;
    SubmitBatchMethod SubmitBatch = (SubmitBatchMethod) config.GetSetting("SubmitBatchMethod").AsPointer;
    CheckBatchStatusMethod CheckBatchStatus = (CheckBatchStatusMethod) config.GetSetting("CheckBatchStatusMethod").AsPointer;
    BatchCompleteMethod CompleteBatch = (BatchCompleteMethod) config.GetSetting("BatchCompleteMethod").AsPointer;

    SetupBatch(config, Chromosomes, CurrentGeneration);

    while (retry_count > 0) {
        
        grid_id = SubmitBatch(config, Chromosomes, CurrentGeneration);

        Status = CheckBatchStatus(grid_id);

        while (Status == Queued || Status == Running) {
            sleep(1);
            Status = CheckBatchStatus(grid_id);
        }

        if (CompleteBatch(grid_id, config, Chromosomes, CurrentGeneration)) {
            break;
        }
     
        std::cerr << "Batch failed grid execution. Retrying..." << std::endl;

    }

    if (retry_count == 0) {
        std::cerr << "Batch failed grid execution. Exiting..." << std::endl;
        exit(1);
    }
}

}

}
