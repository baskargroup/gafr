#include <stdio.h>
#include <fstream>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include <functional>

#include "ga.h"


namespace GA {
namespace Evaluation {

class semaphore {
private:
    std::mutex sem_lock;
    std::condition_variable sem_cv;
    unsigned long count;

public:
    semaphore() : count(0) { }

    void notify() {
        std::unique_lock<std::mutex> lock(sem_lock);
        count++;
        sem_cv.notify_one();
    }

    void wait() {
        std::unique_lock<std::mutex> lock(sem_lock);
        while(!count) sem_cv.wait(lock);
        --count;
    }

    void wait(int n) {
        while(n-- > 0) {
            std::unique_lock<std::mutex> lock(sem_lock);
            while(!count) sem_cv.wait(lock);
            --count;
        }
    }

};

struct job {
    bool stop;
    Chromosome * ch;
};

void ThreadEntry(void * thread_data);

class GAThreadPool {
private:
    std::vector<std::thread> threads;
    std::queue<job> job_queue;
    std::mutex queue_lock;
    semaphore queue_sem;
    semaphore queue_clear;

    unsigned int nThreads;
    unsigned int counter;

    GAThreadPool() : threads(), job_queue(), queue_sem(), queue_clear(), nThreads(0), counter(0), setup(false) { }

    ~GAThreadPool() {
        for(int i = 0; i < nThreads; i++) {
            job j;
            j.stop = true;
            j.ch = 0;
            
            job_queue.push(j);
            queue_sem.notify();
        }
        
        for(auto& thr : threads) {
            thr.join();
        }
    }


public:
    static GAThreadPool& Instance() {
        static GAThreadPool tp;
        return tp;
    }

    void Init(const Configuration& config) {
        nThreads = (unsigned int) config.GetSetting("NumThreads").AsInt;
        CreateNewThreadData create_data = [](int id) { return reinterpret_cast<void *>(id); };
        
        if(config.HasSetting("CreateNewThreadDataMethod")) {
            create_data = * (CreateNewThreadData *) config.GetSetting("CreateNewThreadDataMethod").AsPointer;
        }
        
        
        for(int i = 0; i < nThreads; i++) {
            threads.push_back( std::thread(ThreadEntry, create_data(i)) );
        }

        setup = true;
    }

    void PushJob(Chromosome * ch) {
        job j;
        j.stop = false;
        j.ch = ch;
        
        std::unique_lock<std::mutex> lock(queue_lock);
        job_queue.push(j);
        queue_sem.notify();
    }

    job PopJob() {
        queue_sem.wait();
        std::unique_lock<std::mutex> lock(queue_lock);
        
        job j = job_queue.front();
        job_queue.pop();
        return j;
    }
    
    void Notify() {
        queue_clear.notify();
    }

    void Wait(int njobs) {
        queue_clear.wait(njobs);
    }

    bool setup;

};

void ParallelEvaluate(const Configuration& config, std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration) {

    GAThreadPool& tp = GAThreadPool::Instance();

    if (!tp.setup) {
        std::cerr << "Starting thread pool with " << config.GetSetting("NumThreads").AsInt << " threads...";
        tp.Init(config);
        std::cerr << "Done" << std::endl;
    }

    for(int idx : CurrentGeneration) {
        tp.PushJob(Chromosomes[idx]);
    }

    tp.Wait(CurrentGeneration.size());
}


void ThreadEntry(void * thread_data) {
    while (true) {
        job j = GAThreadPool::Instance().PopJob();
        if(j.stop) break;
        if(j.ch) {
            j.ch->SetThreadData(thread_data);
            j.ch->GetScore();
        }
        GAThreadPool::Instance().Notify();
    }
}

}
}