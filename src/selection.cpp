#include "ga.h"

namespace GA {
namespace Selection {
    
Chromosome * TournamentSelect(const Configuration& config, const std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration) {
    int k = config.GetSetting("TournamentSize").AsInt;
    Chromosome * parent;
    int i, j, size = CurrentGeneration.size();

    j = (int) (size * Random::GetRandom());
    parent = Chromosomes[CurrentGeneration[j]];

    for(i = 0; i < k; i++) {
        j = (int) (size * Random::GetRandom());

        if(Chromosomes[CurrentGeneration[j]]->GetScore() > parent->GetScore())
            parent = Chromosomes[CurrentGeneration[j]];
    }

    return parent;
}

Chromosome * RouletteWheelSelect(const Configuration& config, const std::vector<Chromosome *>& Chromosomes, Generation& CurrentGeneration) {
    bool ranked = false;
    double sum = 0;
    int popSize = CurrentGeneration.size();

    if (config.HasSetting("RouletteWheelRanked")) {
        ranked = config.GetSetting("RouletteWheelRanked").AsBool;
    }

    // Find sum of scores or ranks.
    if (ranked) {
        sum = popSize * (popSize + 1) / 2;
    }
    else {
        for (int idx : CurrentGeneration) {
            sum += Chromosomes[idx]->GetScore();
        }
    }
    
    // Pick a parent.
    double sum_part = 0;
    double random = Random::GetRandom() * sum;
    int i;

    for (i = 0; i < popSize; i++) {
        if (ranked) {
            sum_part += i + 1;
        }
        else {
            sum_part += Chromosomes[CurrentGeneration[i]]->GetScore();
        }

        if (sum_part > random) break;
    }

    //
    // N.B. The above should work given reasonable fitness values. If
    //      client code produces NaN's or Inf's then the above may 
    //      produce i == popSize. Check here and correct it.
    //

    if (i >= popSize) {
        i = popSize - 1;
    } 

    return Chromosomes[CurrentGeneration[i]];
}



}
}
