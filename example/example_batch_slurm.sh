#!/bin/bash
#SBATCH -J gridga_example
#SBATCH -o job.out
#SBATCH -e job.err
#SBATCH -n 1
#SBATCH -p normal
#SBATCH -t 00:05:00

for Line in $(cat input) ; do
    IDX=$(echo $Line | sed "s/,/ /g" | awk '{print $1}')
    A=$(echo $Line | sed "s/,/ /g" | awk '{print $2}')
    OUT=$(python -c "print $A*$A")
    echo "$OUT" > $IDX
done
