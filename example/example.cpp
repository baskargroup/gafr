#include <ga.h>
#include <math.h>
#include <time.h>
#include <stdio.h>

using namespace GA;

class XYValue : public Chromosome {
private:
    double x_coord;

protected:

    virtual double Evaluate() {
        // Calculate y = f(x) = x^2
        double y = x_coord * x_coord;

        // Invert the fitness so that higher scores
        // correspond to lower y values
        return exp(-y);
    }

public:
    virtual void Generate() {
        x_coord = Random::GetRandom() * 10000.0 - 5000.0;
    }

    virtual Chromosome * Clone() {
        XYValue * clone = new XYValue();
        CloneBase(clone);
        clone->SetCoord(x_coord);
        return clone;
    }

    virtual void Mutate() {
        x_coord += (Random::GetRandom() * 4.0 - 2.0);
    }

    double GetCoord() { return x_coord; }
    void SetCoord(double val) { x_coord = val; }
};

Chromosome * XYValueAllocator() {
    return (Chromosome *) new XYValue();
}

void StepCallback(Configuration& config) {
    // Do anything here that needs to be done after each generation.
}

void XYValueCrossover(const Configuration& config, Chromosome * child, const Chromosome * parent1, const Chromosome * parent2) {
    double val1 = ((XYValue *) parent1)->GetCoord();
    double val2 = ((XYValue *) parent2)->GetCoord();
    ((XYValue *) child)->SetCoord((val1 + val2) / 2.0);
}

int main(int argc, char * argv[]) {
    std::cout << "Example started" << std::endl;

    int PopulationSize = 40;

    if(argc == 2) {
        sscanf(argv[1], "%d", &PopulationSize);
    }

    std::cout << "Population Size: " << PopulationSize << std::endl;

    // Use static seed for reproducibility
    Random::Reset(0xDEADBEEF);

    // Setup
    Solver s(PopulationSize, XYValueCrossover, XYValueAllocator);

    // Optional:
    s.Config().SetSetting("NumThreads", 20);
    s.Config().SetSetting("EvaluationMethod", (void *) Evaluation::ParallelEvaluate);
    s.Config().SetSetting("StepCallback", (void *) StepCallback);

    s.Config().PrintConfiguration();

    // Run it
    s.Run(true);

    // View Results
    std::cout << "Completed GA." << std::endl;
    XYValue * xyv = (XYValue *) s.GetTop();
    std::cout << "Top score: " << xyv->GetScore() << " at x = " << xyv->GetCoord() << std::endl;

    return 0;
}
