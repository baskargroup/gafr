#include <math.h>
#include <time.h>
#include <stdio.h>
#include <sstream>

#include "gridga.h"
#include "gridutil.h"

using namespace GA;
using namespace GA::GRID;
using namespace GRIDUTIL;

/// Not the best way to do this but hey, it's 1:28 AM and I want this to work before I got to bed.
#define BASEDIR "/work/02866/medavies/gafr/"

class XYValue : public GridJobChromosome {
private:
    double x_coord;

public:
    virtual void Generate() {
        x_coord = Random::GetRandom() * 10000.0 - 5000.0;
    }

    virtual Chromosome * Clone() {
        XYValue * clone = new XYValue();
        CloneBase(clone);
        clone->SetCoord(x_coord);
        return clone;
    }

    virtual void Mutate() {
        x_coord += (Random::GetRandom() * 4.0 - 2.0);
    }

    virtual void SetupJob() {
        std::stringstream cmd;
        std::stringstream filename;

        std::cerr << "Job setup" << std::endl;

        cmd << "mkdir -p data/" << id;
        sh(cmd.str().c_str());

        filename << "data/" << id << "/input";
        std::ofstream ofs(filename.str());
        ofs << x_coord;
        ofs.close();

    }

    virtual GridStatus SubmitJob() {
        std::stringstream script;
        std::stringstream dir;

        script << BASEDIR << "example/example_slurm.sh";
        dir << BASEDIR << "data/" << id << "/";

        grid_id = SlurmSubmitJob(script.str(), dir.str());

    return GetStatus();
    }

    virtual GridStatus GetStatus() {
        GridStatus Status = SlurmJobState(grid_id);

        // Here I assume that at this point the job has
        // been submitted. Therefore if it is not found
        // in the grid queues then it must have finished.

        if (Status == NotSubmitted) return Finished;
        return Status;
    }

    virtual bool CheckOutput() {
        std::stringstream filename;
        filename << "data/" << id << "/output";
        std::ifstream ifs(filename.str());

        return ifs.is_open();       
    }

    virtual double ReadResult() {
        double result = -1.0;
        std::stringstream filename;
        filename << "data/" << id << "/output";
        std::ifstream ifs(filename.str());

                ifs >> result;
        return result;
    }

    double GetCoord() { return x_coord; }
    void SetCoord(double val) { x_coord = val; }
};

Chromosome * XYValueAllocator() {
    return (Chromosome *) new XYValue();
}

void XYValueCrossover(const Configuration& config, Chromosome * child, const Chromosome * parent1, const Chromosome * parent2) {
    double val1 = ((XYValue *) parent1)->GetCoord();
    double val2 = ((XYValue *) parent2)->GetCoord();
    ((XYValue *) child)->SetCoord((val1 + val2) / 2.0);
}

int main(int argc, char * argv[]) {
    std::cout << "Example started" << std::endl;

    int PopulationSize = 20;

    std::cout << "Population Size: " << PopulationSize << std::endl;

    // Use static seed for reproducibility
    Random::Reset(0xDEADBEEF);

    // Setup
    Solver s(PopulationSize, XYValueCrossover, XYValueAllocator);

    // When using a HPC grid, it is most appropriate to set this to be equal to the max queue size that the GA will use.
    // Also, don't set this to be greater than the number of population members.
    s.Config().SetSetting("NumThreads", 20);

    s.Config().SetSetting("grid_retry_count", 5);

    // Required or the jobs won't run in parallel
    s.Config().SetSetting("EvaluationMethod", (void *) Evaluation::ParallelEvaluate);

    // Run it
    s.Run(true);

    // View Results
    std::cout << "Completed GA." << std::endl;
    XYValue * xyv = (XYValue *) s.GetTop();
    std::cout << "Top score: " << xyv->GetScore() << " at x = " << xyv->GetCoord() << std::endl;

    return 0;
}
