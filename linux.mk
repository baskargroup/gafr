CXX=g++
INCLUDE= -IdSFMT -Iinclude
CXXFLAGS= -fPIC -static -std=c++11 -g
OBJ=obj/random.o obj/selection.o obj/configuration.o obj/evaluation.o obj/ga.o obj/dSFMT.o obj/pareval.o obj/gridga.o obj/gridutil.o

all: default
debug: default
default: pre-build bin/libgafr.a

.PHONY: example

example: default
	@echo "    [CXX] bin/example"
	@$(CXX) -Lbin -Iinclude -o bin/example example/example.cpp -lgafr -pthread -std=c++11

example_grid: default
	@echo "    [CXX] bin/example_grid"
	@$(CXX) -Lbin -Iinclude -o bin/example_grid example/example_grid.cpp -lgafr -pthread -std=c++11

example_batch_grid: default
	@echo "    [CXX] bin/example_batch_grid"
	@$(CXX) -Lbin -Iinclude -o bin/example_batch_grid example/example_batch_grid.cpp -lgafr -pthread -std=c++11


pre-build:
	@mkdir -p bin
	@mkdir -p obj

bin/libgafr.a: $(OBJ)
	@echo "    [LD] $@"
	@ar rcs $@ $(OBJ)

obj/%.o: src/%.cpp
	@echo "    [CXX] $<"
	@$(CXX) $(INCLUDE) $(CXXFLAGS) -c -o $@  $<


obj/%.o: src/%.c
	@echo "    [CC] $<"
	@$(CXX) $(INCLUDE) $(CXXFLAGS) -c -o $@  $<

clean:
	rm -rf obj/*
	rm -rf bin/*
