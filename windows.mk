### Win.mk for use with Windows NMAKE system
#
# Only the following line should change in most cases:
OBJ=obj/random.obj obj/selection.obj obj/configuration.obj obj/evaluation.obj obj/ga.obj obj/dSFMT.obj obj/pareval.obj
#####################################
# DO NOT MODIFY ANYTHING BELOW HERE
# (Unless you know what you're doing)

INCFLAGS=/IdSFMT /Iinclude
CXXFLAGS=/nologo /EHsc $(OFLAGS)
CFLAGS=/nologo /EHsc $(OFLAGS)

all: default
default: release

release: pre-build
    @nmake /f windows.mk /nologo bin/libgafr.lib OFLAGS=/Ox

debug: pre-build
    @nmake /f windows.mk /nologo bin/libgafr.lib OFLAGS=/Zi DFLAGS="/FS /Fdbin\libgafr.pdb"

.PHONY: example

example: default
	@cl /nologo /Iinclude /EHsc .\example\example.cpp .\bin\libgafr.lib /Foobj\ /Febin\example.exe

pre-build:
	@if NOT EXIST "bin" mkdir "bin"
	@if NOT EXIST "obj" mkdir "obj"

bin/libgafr.lib: $(OBJ)
	@lib /nologo  /OUT:$@ $**

{src}.cpp{obj}.obj::
	@cl $(CXXFLAGS) $(INCFLAGS) /MP $(DFLAGS) /Foobj\ /c $<

{src}.c{obj}.obj::
	@cl $(CFLAGS) $(INCFLAGS) /MP $(DFLAGS) /Foobj\ /c $<

clean:
	@del /Q obj\*
	@del /Q bin\*

